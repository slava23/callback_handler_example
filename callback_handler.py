#dummy application that shows how to expose the required endpoints
from flask import Flask, request
import random
app = Flask(__name__)

# tx_sign_request endpoint:
@app.route('/tx_sign_request', methods=['POST'])
def tx_sign_request():
    # your code should run in here
    request_data = request.get_json()
    txId = request_data['txId']
    try:
        operation = request_data['operation']
        if operation == 'TRANSFER':
            return {
                "action": "APPROVE"
            }
        else:
            return {
                "action": "REJECT",
                "rejectReason": "illegal operation"
            }
    except:
        return "Bad Request: 'operation' doesn't exist or wrong", 400

    

# config_change_sign_request endpoint:
@app.route('/config_change_sign_request', methods=['POST'])
def change_sign_request():
    # your code should run here
    if random.randint(1,2) == 1:
        return {
                "action": "APPROVE"
            }
    else:
        return {
                "action": "REJECT",
                "rejectReason": "some rejection reason"
            }



if __name__ == '__main__':
    import logging
    logging.basicConfig(filename='error.log',level=logging.DEBUG)
    app.run(debug=True)  
